from motor import motor_asyncio as motor
import asyncio
from async_lru import alru_cache

class Database:
    def __init__(self, **kwargs):
        self._client = motor.AsyncIOMotorClient("mongodb://{database_user}@{database_path}".format(**kwargs))
        self._database = self._client[kwargs['database_name']]

        self.servers = _Collection(self, "servers", None)
        self.users = _Collection(self, "users", 2**8)
        self.internal = _InternalCollection(self)

class _Collection:
    def __init__(self, parent, collection_name, cache_size):
        self._parent = parent
        self._name = collection_name
        self._collection = self._parent._database[collection_name]
        self._cache_len = cache_size

    @alru_cache(maxsize=_cache_len)
    async def _prefix(self, id):
        return await self._collection.find_one({'_id': id}, projection=['prefix'])

    def _cache_clear(self):
        _prefix.cache_clear()
        _prefs.cache_clear()

    __getitem__ = _Document

class _Document:
    def __init__(self, parent, id):
        self._parent = parent
        self.id = id
        self.prefix = _Prefix(self)

class _Prefix:
    def __init__(self, parent):
        self._parent = parent

    async def __call__(self):
        return await self._parent._parent._prefix(self._parent.id)

    async def add(self, new):
        assert isinstance(new, str) or isinstance(new, list)
        new = [new] if isinstance(new, str) else new
        await self._parent._parent._collection.update_one({'_id': self._parent.id}, {'$push': {'prefix': {'$each': new}}})
        return self._cache_invalidate()

    async def set(self, new):
        assert isinstance(new, str) or isinstance(new, list)
        new = [new] if isinstance(new, str) else new if isinstance(new, list)
        return await self._parent._parent._collection.update_one({'_id': self._parent.id}, {'$set': {'prefix': new}})

    def _cache_invalidate(self):
        prefix_type = self._parent._parent._name
        self._parent._parent._parent.internal.cache_clear(prefix_type=='servers', prefix_type='users')

class _InternalCollection:
    def __init__(self, parent):
        self._parent = parent
        self._collection = self._parent._database["internal"]

    async def flag_check(self):
        servers_invalid = self._collection.find_one({'_id': 'servers'})['check']
        users_invalid = self._collection.find_one({'_id': 'servers'})['check']
        invalid = await asyncio.gather(servers_invalid, users_invalid)
        self._cache_clear(*invalid)

    def cache_clear(self, servers_invalid=False, users_invalid=False):
        if servers_invalid: self._parent.servers._cache_clear()
        if users_invalid: self._parent.users._cache_clear()
