from discord import *
from discord.ext import commands as cmd
import asyncio
import logging

import glitch
import database

token = glitch.token
global_prefix = "."

db = database.Database(**glitch.database_credentials)

logging.basicConfig(logging.INFO)

async def get_prefix(client, message):
    if type(message.channel) == DMChannel:
        return ""

    prefixes = [client.user.mention]

    server_prefix = db.servers[message.guild.id].prefix()
    user_prefix = db.users[message.author.id].prefix()

    server_prefix, user_prefix = await asyncio.gather(server_prefix, user_prefix)

    prefixes += server_prefix if server_prefix else global_prefix
    if user_prefix: prefixes += user_prefix

    return prefixes

client = cmd.Bot(command_prefix=get_prefix)

@client.event
async def on_ready():
    logging.info("Logged in as {} with ID {}".format(client.user.name, client.user.id))
    await client.change_presence(game=Game("with myself"))

    for ext in ['bot_admin', 'moderation']:
        client.load_extension(f"cogs.{ext}}")

client.run(token)
