from discord.ext import commands as cmd
from discord import *

@cmd.guild_only()
@cmd.command(aliases=['clr','del','purge'])
async def clear(ctx, arg: _Clear):
    if not ctx.guild:
    try:
        num = int(args[0])
    except ValueError:
        num = 100

class _Clear(cmd.Converter):
    async def convert(self, ctx, argument):
