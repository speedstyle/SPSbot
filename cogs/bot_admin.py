from discord import *
from discord.ext import commands as cmd
import glitch

@cmd.command()
async def status(ctx):
    embed = Embed()

    # discord users
    embed.add_field(
        name="Usage info",
        inline=True,
        value="\n".join([
            "Users: **{}**".format(len(ctx.bot.users)),
            "Servers: **{}**".format(len(ctx.bot.guilds)),
            "Commands: **{}**".format(len(ctx.bot.walk_commands))
        ])
    )

    # API connection
    embed.add_field(
        name="Discord API",
        inline=True,
        value="\n".join([
            "Ping: **{}ms**".format(int(1000*ctx.bot.latency()))
        ])
    )

    # resource usage

    ctx.send(embed=embed)

@cmd.is_owner()
@cmd.command()
async def eval(ctx):
    pass

def setup(bot):
    bot.add_command(status)
    bot.add_command(eval)
