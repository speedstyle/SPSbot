from os import environ
import asyncio

from subprocess import Popen
express = Popen(['nohup', 'node keepalive.js'], shell=False)

discord_token = environ["DiscordAPIToken"]

database_credentials = {database_path: environ["MongoDBPath"],
                        database_user: environ["MongoDBUser"],
                        database_name: environ["MongoDBName"]}
