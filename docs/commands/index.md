# Commands

Arguments in `<angular brackets>` are required; arguments in `[square brackets]` are not.

|Command|Description|Usage|
|:---|----------:|-------|
|[`status`](#status)|Shows the bot's current status.|`status`|
|[`clear`](#clear)|Bulk delete messages|`clear 200 #general`|
