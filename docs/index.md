# Welcome to SPSbot

SPSbot is a simple Discord bot written with [`discord.py`][d.py].

If something's broken, tell me in the [repo][repo-new-issue], the [discord server][discord] or - if you have the time, knowledge and inclination - I would be grateful for a [patch][repo].

At present, the bot is not public; it is only available on the aforementioned [discord server][discord]. A public version will be created when it can do something.

For more information on how to use the bot, see the [commands page](commands). For more information on the backend, see the docs for [`discord.py`][d.py] and the [source][repo].

[d.py]: https://github.com/Rapptz/discord.py
[repo]: https://github.com/speedstyle/SPSbot
[repo-new-issue]: https://gitlab.com/speedstyle/SPSbot/issues/new
[discord]: https://discord.gg/UGCbJcM
